package de.audioattack.openlink;

/**
 * Created by low012 on 23.10.17.
 */

public class IncognitoBrowser {

    public final String packageName;
    public final String browserActivityName;
    public final String incognitoExtra;
    public final int minVersionCode;

    public IncognitoBrowser(final String packageName,
                            final String browserActivityName,
                            final int minVersionCode,
                            final String incognitoExtra) {
        this.packageName = packageName;
        this.browserActivityName = browserActivityName;
        this.minVersionCode = minVersionCode;
        this.incognitoExtra = incognitoExtra;
    }
}
